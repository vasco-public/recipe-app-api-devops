#!/bin/sh

# Exit immediatly if anything goes wrong
set -e

# --noinput surpress any questions askes default to yes
python manage.py collectstatic --noinput


# Polls the db each second untill it's available
python manage.py wait_for_db
python manage.py migrate

# Run uwsgi as as a TCO socket on port 9001
# --master: Run this as the master service, means that when
# It gets the exit signal, docker will try to close it gracefully
uwsgi --socket :9001 --workers 4 --master --enable-threads --module app.wsgi






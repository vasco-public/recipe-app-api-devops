#!/bin/bash


sudo yum update -y
sudo yum install -y amazon-linux-extras
sudo amazon-linux-extras enable docker
sudo yum -y install docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo usermod -aG docker ec2-user


resource "aws_lb" "api" {
  name               = "${local.prefix}-main"
  load_balancer_type = "application"

  # NEEDS to be in public subnets, as it will be accessed from the internet
  subnets = [
    aws_subnet.public_a.id,
    aws_subnet.public_b.id
  ]

  security_groups = [
    aws_security_group.lb.id
  ]

  tags = local.common_tags
}

# Target group is the group of servers the lb can forward requests to
resource "aws_lb_target_group" "api" {

  # Target group for our api application
  name   = "${local.prefix}-api"
  vpc_id = aws_vpc.main.id

  # The LB will send requests to the api through HTTP on port 8000  
  protocol = "HTTP"
  port     = 8000

  target_type = "ip"


  # Health check for the target group, which checks by calling a path/url
  # periodicly to make sure that the application still is healthy or not
  # If it's not healthy (not 200 res), then it tries to restart it 
  health_check {
    path = "/admin/login/"
  }
}

# The HTTP requests need to be redirect to HTTPs
resource "aws_lb_listener" "api" {
  load_balancer_arn = aws_lb.api.arn

  port     = 80
  protocol = "HTTP"

  # Redirect to HTTPS lb listner on port 443
  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}


# Listner: entrypoints into the load balancer, which is then send to the target group
# it accepts the requests to our load balancer
resource "aws_lb_listener" "api_https" {
  load_balancer_arn = aws_lb.api.arn

  # HTTPs on the right port and ofcourse the certificated need for the SSL connection
  port     = 443
  protocol = "HTTPS"

  # We grab the cert from the validation resource and NOT the certificate resource, why?
  # - We need to make sure the validation has happened, before we can reference it
  # - In the graph tf creates, it will make sure to run this AFTER the cert is validated
  certificate_arn = aws_acm_certificate_validation.cert.certificate_arn


  # Forward requests to the target group
  # default_action: How to handle incoming requests
  # The target group contains the list of IP adresses of our running tasks it forwards to
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.api.arn
  }
}


resource "aws_security_group" "lb" {
  description = "Allow access to Application Load Balancer"
  name        = "${local.prefix}-lb"
  vpc_id      = aws_vpc.main.id

  # Incoming requests from the internet, so from any ip
  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allows HTTPs requests from the internet
  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Access our LB has to our application, so it can connect on
  # port 8000 into the application as the nginx has that one open
  egress {
    protocol    = "tcp"
    from_port   = 8000
    to_port     = 8000
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = local.common_tags
}
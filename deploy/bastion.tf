
# The AMI is the image with the operating system that is going to run on it
# We use the a linux image, as it's most optimized to run on EC2

# Syntaxt "data service_name given_name"
# data "aws_ami" means we are retrieving data from the AWS AMI service on amazon
# The "amazon_linux" part is just the name we give this to access in the resource block
data "aws_ami" "amazon_linux" {

  most_recent = true

  # We need to specify which image is going to be used to create the instance
  # We do this by filtering the images by name
  filter {
    name = "name"

    # Makes sure we have the latest version with all the latest security patches
    # In this image we replace the minor version with a wildcard
    values = ["amzn2-ami-kernel-5.10-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}

# Create the bastion role with the sts assumerole attached
resource "aws_iam_role" "bastion" {
  name               = "${local.prefix}-bastion"
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags
}

# With this we attach the AmazonEC2ContainerRegistryReadOnly policy to the role
resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  role       = aws_iam_role.bastion.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

# Now we can create the instance profile, so the profile assigned to bastion 
# when it launches. So like a temporary role that is needed for deployment
resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}


resource "aws_instance" "bastion" {

  # Now grab the id that was grabbed from AWS with the data{} block
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"

  # User data script is a script that runs when the instance is created
  # This will install and setup docker on the EC2 bastion instance
  user_data = file("./templates/bastion/user-data.sh")

  # The instance profile we created with the sts assume and ec2 readonline permissions
  iam_instance_profile = aws_iam_instance_profile.bastion.name

  # The ssh key it needs to access the instance
  key_name = var.bastion_key_name

  # We launch the instance in the public subnet a, so it's accisble through internet
  # We use the subnet id from the public_a subnet, as it's not as critical only admin access
  subnet_id = aws_subnet.public_a.id

  # Cusotm security group with inbound and outbound rules
  vpc_security_group_ids = [aws_security_group.bastion.id]

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,

    # This refers to the locals {} block in the main.tf file
    # Which itself uses the static variable in the variables.tf file
    # e.g. raad-dev-bastion or raad-prod-bastion
    map("Name", "${local.prefix}-bastion")
  )
}

resource "aws_security_group" "bastion" {
  description = "Control bastion inbound and outbound access"
  name        = "${local.prefix}-bastion"

  vpc_id = aws_vpc.main.id

  # Inbound rules - SSH
  ingress {
    description = "Allow SSH access from anywhere"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Prod should only add IPs that need access
  }

  # Outbound rules - HTTPs
  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outbound rules - HTTP
  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outbound rules - Postgres
  egress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432

    # Only alow access from the private subnets
    # So bastion server can connect through private subnet to database
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  tags = local.common_tags
}
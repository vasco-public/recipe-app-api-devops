# This will create a simple cluster per each environment
resource "aws_ecs_cluster" "main" {

  # local.prefix contains the environment name, so 1 per env
  name = "${local.prefix}-cluster"

  tags = local.common_tags
}

# Will be used to assign the task excution role to the cluster
resource "aws_iam_policy" "task_execution_role_policy" {
  name        = "${local.prefix}-task-execution-role-policy"
  path        = "/"
  description = "Allows retrieving of images and adding to logs"
  policy      = file("./templates/ecs/task-exec-role.json")
}

# This the role used to run the task/container, so writing to logs etc
resource "aws_iam_role" "task_execution_role" {
  name = "${local.prefix}-task-execution-role"

  # This is the role that allows ECS to assume a role
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

# Now we attach the policy for execution to the role
resource "aws_iam_role_policy_attachment" "task_execution_role" {
  role       = aws_iam_role.task_execution_role.name
  policy_arn = aws_iam_policy.task_execution_role_policy.arn
}

# Will be used to give permissions to our task that it needs at runtime (after it's deployed)
# The policies for this will be extended for everything our container needs access to
resource "aws_iam_role" "app_iam_role" {
  name = "${local.prefix}-api-task"

  # This is the role that allows ECS to assume a role
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-api"

  tags = local.common_tags
}


# Here pass the variables to the template file for task definition 
# that will be used to run our containers. 
# NOTE: Locally make sure to run terraform init, as it needs another provider
data "template_file" "api_container_definitions" {

  # Grab the template file, so we can source variables from it
  template = file("./templates/ecs/container-definitions.json.tpl")

  vars = {

    # URIs for the images we built and pushed to ECR
    app_image   = var.ecr_image_api
    proxy_image = var.ecr_image_proxy

    # Secret key which will be set in the pipeline from a secret 
    django_secret_key = var.django_secret_key

    # Here we can directly source the db creds from the RDS instance
    db_host = aws_db_instance.main.address
    db_name = aws_db_instance.main.name
    db_user = aws_db_instance.main.username
    db_pass = aws_db_instance.main.password

    # Grab this from the loggroup resource we created above
    log_group_name   = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region = data.aws_region.current.name

    # The allow host are set to our load balancer address
    allowed_hosts = aws_route53_record.app.fqdn

    # s3 vars needed for django as env vars for static storage
    s3_storage_bucket_name   = aws_s3_bucket.app_public_files.bucket
    s3_storage_bucket_region = data.aws_region.current.name
  }
}

resource "aws_ecs_task_definition" "api" {

  # The name of the task definition
  family = "${local.prefix}-api"

  # This retrieves our rendered container definition from the template file we defined above
  # So what we do here is reference the "data" block which can render the file with the vars it set
  container_definitions = data.template_file.api_container_definitions.rendered

  # We want to make our ECS task compatible with Fargate
  # Fargate allows us to host containers without managing servers
  requires_compatibilities = ["FARGATE"]

  # We want to use our own vpc so we can connect to databses
  network_mode = "awsvpc"

  # Number of cpu units used by the task
  cpu = 256

  # Amount (in MiB) of memory used by the task
  memory = 512

  # Gives the permissions to excecute a new task to the cluster
  execution_role_arn = aws_iam_role.task_execution_role.arn

  # This is the role given to the actual running task
  task_role_arn = aws_iam_role.app_iam_role.arn

  volume {
    name = "static"
  }

  tags = local.common_tags
}


resource "aws_security_group" "ecs_service" {

  description = "Access of the  ECS service"
  name        = "${local.prefix}-ecs-service"

  vpc_id = aws_vpc.main.id

  # Outbound HTTPs - Allows to pull our images from ECR and download packages etc
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outbound - Postgres through our private subnet network
  egress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  # Inbound - Connections from the public internet to our proxy container
  ingress {
    from_port = 8000
    to_port   = 8000
    protocol  = "tcp"

    # This is the load balancer has inbound access to our ECS service
    security_groups = [
      aws_security_group.lb.id,
    ]
  }

  tags = local.common_tags
}


# This is the service that will run our tasks
resource "aws_ecs_service" "api" {
  name = "${local.prefix}-api"

  # The cluster it is running on
  cluster = aws_ecs_cluster.main.name

  # We pass the task definition by the family name, for the api
  task_definition = aws_ecs_task_definition.api.family

  # So how many tasks we want to run inside this service
  desired_count = 1

  # Allows you to manage the tasks witouth managing the servers
  launch_type = "FARGATE"

  # Needed change: https://gitlab.com/LondonAppDev/recipe-app-api-devops/-/commit/416c01e487cadbf05080d678992403a0910f4b73#e0ca724c1c3b583b098b6de4b61a4e679307386c_115_115
  platform_version = "1.4.0"


  network_configuration {

    # Only allows communication over the private subnets
    subnets = [
      aws_subnet.private_a.id,
      aws_subnet.private_b.id,
    ]

    # This is the security group we created above
    security_groups = [
      aws_security_group.ecs_service.id,
    ]
  }

  # This tells ECS to register new tasks to the lb target group
  # And tell the lb to forward to the container called "proxy"
  load_balancer {
    target_group_arn = aws_lb_target_group.api.arn
    container_name   = "proxy"
    container_port   = 8000
  }

  # Here we make sure the https listener is created before the ECS service
  depends_on = [aws_lb_listener.api_https]
}

# Policy for the ecs service to access s3 bucket for django media files
resource "aws_iam_role_policy_attachment" "ecs_s3_access" {
  role       = aws_iam_role.app_iam_role.name
  policy_arn = aws_iam_policy.ecs_s3_access.arn
}
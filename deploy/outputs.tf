output "db_host" {
  value = aws_db_instance.main.address
}


# Public DNS of the bastion host, so we can connect to the server
output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

# The domain that was created in Route53, which the lb accepts requests from
output "api_endpoint" {
  value = aws_route53_record.app.fqdn
}


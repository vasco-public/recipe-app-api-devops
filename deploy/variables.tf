
# We use a prefix for all our names, so in the console we know what
# belongs to our project. This is a good practice to follow.
variable "prefix" {
  type = string

  # Recipe app api devops
  default     = "raad"
  description = "Abbreviation of our project"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "vasco@bg.com"
}

variable "db_username" {
  description = "Username for db instance"
}

variable "db_password" {
  description = "Password for db instance"

}

# The name of the ssh key it needs to access the bastion host
variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

# Contains the ecr image url for the api container
# This will be overwritten in our deployment pipeline
variable "ecr_image_api" {
  description = "ECR image for api"

  # Copied from the console and ADDED the :latest, which pulls it from that tag
  # The default value is used for when you want to run this locally and not from pipeline 
  default = "348513603635.dkr.ecr.eu-west-1.amazonaws.com/recipe-app-api-devops:latest"
}

# Contains the ecr image url for the proxy container
# This will be overwritten in our deployment pipeline
variable "ecr_image_proxy" {
  description = "ECR image for proxy"

  default = "348513603635.dkr.ecr.eu-west-1.amazonaws.com/recipe-app-api-proxy:latest"
}


variable django_secret_key {
  description = "Secret key for Django app"
}


variable "dns_zone_name" {
  description = "Domain name"
  default     = "coursedev.docs.billygrace.com"
}

variable "subdomain" {
  description = "Subdomain per environment"

  # Create a map entry for each of our environments
  type = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}



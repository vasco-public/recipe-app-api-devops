terraform {

  # You really want to set a remote bucket, as else tf won't have 
  # any idea of what has already been created by another device
  backend "s3" {
    bucket = "recipe-app-api-devops-tfstate-vazkir"

    # Name of the file we want to save the tf state to 
    key = "recipe-app.tfstate"

    region = "eu-west-1"

    # Encrypt the file using the AWS KMS service
    encrypt = true

    # To avoid any conflicts of running it accidentally twice
    # As it locks the current running deployement so that no other can run
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }


}

# You can use it for many different cloud providers
provider "aws" {

  # You want to lock the version, as you don't want to run into any issues
  # Sometimes the new version of the provider can break your code
  version = "~> 4.67.0"

  region = "eu-west-1"
}

# Locals allows you dynamically create variables
# The variable {} block does not allow for dynamic creation
locals {

  # E.g. raad-dev or raad-prod
  # So var.prefix grabs the variable "prefix" {} from the variable.tf
  prefix = "${var.prefix}-${terraform.workspace}"

  # These are the tags we want to apply to any resource we create
  # Allows you to understand easily in the console who, and what
  # Which also means that somebody knows they should not change it as tf manages it
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

# Doesn't need any attributes, it gives us a resource to use to retrieve the current region 
# So you don't need to harcode this in more places then this file
data "aws_region" "current" {}
resource "aws_vpc" "main" {
  # The way to shorten the netmask of IPs that can be used in the VPC
  # We want to create a VPC with the most available IPs possible
  # 10.1 means we want all our internal IPs to start with this
  cidr_block = "10.1.0.0/16"

  # Enables dns inside our VPC
  enable_dns_support = true

  # Enables hostnames on any resources we create inside the VPC
  # This allows us to reference other resources by name within the VPC
  enable_dns_hostnames = true

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,

    # This refers to the locals {} block in the main.tf file
    # Which itself uses the static variable in the variables.tf file
    # e.g. raad-dev-bastion or raad-prod-vpc
    map("Name", "${local.prefix}-vpc")
  )
}

resource "aws_internet_gateway" "main" {
  # VPC to attach this to
  vpc_id = aws_vpc.main.id


  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

####################################################
# Public Subets - Inbound/Outbound Internet Access #
####################################################

resource "aws_subnet" "public_a" {
  # VPC to attach this to
  vpc_id = aws_vpc.main.id

  # /24 gives us 2054 hosts we can create for the IP starting with 10.1.1.*
  cidr_block = "10.1.1.0/24"

  # Any instance launched within this public subnet will get a public IP assigned to it
  map_public_ip_on_launch = true

  # Usually you have an a or b to seperate availability zone per region
  availability_zone = "${data.aws_region.current.name}a"

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-a")
  )
}

resource "aws_route_table" "public_a" {
  # VPC to attach this to
  vpc_id = aws_vpc.main.id

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-a")
  )
}

# Associate the route table we create with our public subnet in the VPC
resource "aws_route_table_association" "public_a" {
  subnet_id      = aws_subnet.public_a.id
  route_table_id = aws_route_table.public_a.id
}

# Maks the subnet accisable to the public internet
resource "aws_route" "public_internet_access_a" {
  route_table_id = aws_route_table.public_a.id

  # This is the cidr block you need to use for the public internet (all addresses)
  destination_cidr_block = "0.0.0.0/0"

  # So the route table knows to use the internet gateway we created
  gateway_id = aws_internet_gateway.main.id

}

# AWS Elastic IP (EIP) provides a static IP address that can be attached to an instance
resource "aws_eip" "public_a" {
  vpc = true

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-a")
  )
}

# Network Address Translation (NAT) Gateway -> Only outbound access to the internet
# Used for our private subnets to access the internet through the public subnets
resource "aws_nat_gateway" "public_a" {
  # The subnet we want to create the NAT gateway in
  allocation_id = aws_eip.public_a.id
  subnet_id     = aws_subnet.public_a.id

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-a")
  )
}

resource "aws_subnet" "public_b" {

  # We put our b side public resources on the 10.1.2.* block
  cidr_block = "10.1.2.0/24"

  vpc_id = aws_vpc.main.id

  # Any instance launched within this public subnet will get a public IP assigned to it
  map_public_ip_on_launch = true

  # Usually you have an a or b to seperate availability zone per region
  availability_zone = "${data.aws_region.current.name}b"

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-b")
  )
}


resource "aws_route_table" "public_b" {
  vpc_id = aws_vpc.main.id

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-b")
  )
}

resource "aws_route_table_association" "public_b" {
  subnet_id      = aws_subnet.public_b.id
  route_table_id = aws_route_table.public_b.id
}


resource "aws_route" "public_internet_access_b" {
  route_table_id = aws_route_table.public_b.id

  # This is the cidr block you need to use for the public internet (all addresses)
  destination_cidr_block = "0.0.0.0/0"

  # So the route table knows to use the internet gateway we created
  gateway_id = aws_internet_gateway.main.id
}

resource "aws_eip" "public_b" {
  vpc = true

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-b")
  )
}

resource "aws_nat_gateway" "public_b" {
  # The subnet we want to create the NAT gateway in
  allocation_id = aws_eip.public_b.id
  subnet_id     = aws_subnet.public_b.id

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-public-b")
  )
}


##################################################
# Private Subets - Outbound Internet Access only #
##################################################


resource "aws_subnet" "private_a" {
  # We put our a side private resources on the 10.1.10.* block
  # Why go to 10 and not to 3 first, as this allows us to 
  # add more public subnets in the future if we need to
  cidr_block = "10.1.10.0/24"

  vpc_id = aws_vpc.main.id

  availability_zone = "${data.aws_region.current.name}a"

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-private-a")
  )
}

resource "aws_route_table" "private_a" {
  vpc_id = aws_vpc.main.id

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-private-a")
  )
}

resource "aws_route_table_association" "private_a" {
  subnet_id      = aws_subnet.private_a.id
  route_table_id = aws_route_table.private_a.id
}

# It has to go through our public network to get to the internet (NAT Gateway)
resource "aws_route" "private_a_internet_out" {
  route_table_id = aws_route_table.private_a.id
  nat_gateway_id = aws_nat_gateway.public_a.id

  # The public internet to access outbound
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_subnet" "private_b" {
  # Now move from 10 to 11
  cidr_block = "10.1.11.0/24"

  vpc_id = aws_vpc.main.id

  availability_zone = "${data.aws_region.current.name}b"

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-private-b")
  )
}


resource "aws_route_table" "private_b" {
  vpc_id = aws_vpc.main.id

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-private-b")
  )
}

resource "aws_route_table_association" "private_b" {
  subnet_id      = aws_subnet.private_b.id
  route_table_id = aws_route_table.private_b.id
}

resource "aws_route" "private_b_internet_out" {

  # So this ones connects to the public b subnet
  route_table_id = aws_route_table.private_b.id
  nat_gateway_id = aws_nat_gateway.public_b.id

  # The public internet to access outbound
  destination_cidr_block = "0.0.0.0/0"
}
resource "aws_s3_bucket" "app_public_files" {
  bucket = "${local.prefix}-vazkir-files"

  # By default when you create a bucket, it doesn;t allow you to destroy it if it's not empty
  # So now it allows you to destroy you bucket very easily even if it's not empty
  force_destroy = true
}

# Fixes from: https://www.udemy.com/course/devops-deployment-automation-terraform-aws-docker/learn/lecture/19174242#questions/19654662
resource "aws_s3_bucket_ownership_controls" "app_public_files" {
  bucket = aws_s3_bucket.app_public_files.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}
resource "aws_s3_bucket_public_access_block" "app_public_files" {
  bucket = aws_s3_bucket.app_public_files.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}


# Create the policy from the template file by passing in the bucket ARN
data "template_file" "ecs_s3_write_policy" {
  template = file("./templates/ecs/s3-write-policy.json.tpl")

  vars = {
    bucket_arn = aws_s3_bucket.app_public_files.arn

  }
}

resource "aws_iam_policy" "ecs_s3_access" {
  name        = "${local.prefix}-AppS3AccessPolicy"
  path        = "/"
  description = "Allow ECS tasks to access recipe api S3 bucket"

  # The data{} block's rendered json is the policy document
  policy = data.template_file.ecs_s3_write_policy.rendered
}
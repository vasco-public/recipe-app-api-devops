# Subnet group where the DB will live inside the VPC
resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"

  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id,
  ]

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

# Allows inbound access to the DB from the VPC at port 5432
resource "aws_security_group" "rds" {
  description = "Allow access to RDS database instance"
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id

  # inrgess controls what the rules are for the inbound access
  # No need to for egress as it does not need to connect to outbound resources
  ingress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432

    # Means access will be limited to resources that have this security group assigned to it
    # So now only the bastion security group access access the database
    security_groups = [
      aws_security_group.bastion.id,    # Bastion needs to access the DB
      aws_security_group.ecs_service.id # ECS service needs to access the DB
    ]
  }

  tags = local.common_tags
}

resource "aws_db_instance" "main" {
  identifier = "${local.prefix}-db"

  # Within the posgress server you can have multiple databases
  db_name = "recipe"

  # 20GB of storage
  allocated_storage = 20

  # gp2: General Purpose SSD, entry level storage type (cheaper)
  storage_type   = "gp2"
  engine         = "postgres"
  engine_version = "11.21"

  # Lower cost db service, changes memory and cpu assigned to db
  instance_class          = "db.t2.micro"
  db_subnet_group_name    = aws_db_subnet_group.main.name
  username                = var.db_username
  password                = var.db_password
  backup_retention_period = 0

  # It increases the costs if you have it in multiple zones
  multi_az = false

  # Want to skip as we are just testing with terraform
  # TF needs a unique name for the snapshot, so we can't use the same name
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.rds.id]

  # This allows merging a dictionary with new entries like "Name"
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

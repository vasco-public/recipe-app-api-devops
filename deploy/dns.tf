# A data reference to retrieve the Route53 zone name
data "aws_route53_zone" "zone" {

  # They write the zone name with a trailing dot, so we need to add it here
  name = "${var.dns_zone_name}."
}

# Create a CNAME record for the api domain name that will be attached to the load balancer
resource "aws_route53_record" "app" {

  # The master domain name zone, where we will be adding records for the subdomains to
  zone_id = data.aws_route53_zone.zone.zone_id

  # loopup accepts 2 parameters, the map and the key to lookup, so that the map uses to output a string
  # e.g. api.staging and mydomain.com => api.staging.mydomain.com
  name = "${lookup(var.subdomain, terraform.workspace)}.${data.aws_route53_zone.zone.name}"

  # CNAME -> Canonical Name which bascially links it to another domain name 
  type = "CNAME"
  ttl  = "300"

  # So the Load Balancer is what the main api domain will be pointing to
  records = [aws_lb.api.dns_name]
}


resource "aws_acm_certificate" "cert" {

  # The domain name that we want to get a certificate for
  # fqdn -> Fully Qualified Domain Name
  domain_name       = aws_route53_record.app.fqdn
  validation_method = "DNS"

  # This needs to be added for our tf to run smooth, otherwise
  # there will be an error given when you destroy it
  lifecycle {
    create_before_destroy = true
  }

  tags = local.common_tags
}

# Create a DNS record for the certificate validation
resource "aws_route53_record" "cert_validation" {

  # The master domain name zone, where we will be adding records for the subdomains to
  zone_id = data.aws_route53_zone.zone.zone_id

  # We need to grab the random subdomain domain string the acm created and add it to the route53 record
  name = tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_name
  type = tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_type

  # It needs to be pointed to the certificate validation record value
  records = [
    tolist(aws_acm_certificate.cert.domain_validation_options)[0].resource_record_value,
  ]

  ttl = "60"
}


# 'aws_acm_certificate_validation' isn't a real AWS resource/service, it is used
# to start the validation process in our DNS records
resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [aws_route53_record.cert_validation.fqdn]
}
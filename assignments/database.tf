#############################
# Database setup Assignment #
#############################

# Instructions:
#   Create a new database instance with the following specifications:
#     - Uses the MySQL engine (version 5.7)
#     - Runs as a `db.t2.micro` class
#     - Has the default username: `user`
#     - Has the default password: `password`
#     - Has 5GB of allocated storage
#     - Has the identifier `assignment-db`
#     - Set skip final snapshot to `true`

# Notes:
#  - Add all code within `aws_db_instance` below
#    (no need to create Variables, VPC, Subnets, Security Groups, etc...)
#  - Use official docs as a guide:
#    https://www.terraform.io/docs/providers/aws/r/db_instance.html


##########################
# Main (Terraform Setup) #
##########################

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

############
# Database #
############

resource "aws_db_instance" "database" {
  instance_class = "db.t2.micro"

  ## --- INSERT CODE HERE --- ##
  username                = "user"
  password                = "password"

  allocated_storage = 5

  identifier = "assignment-db"

  skip_final_snapshot    = true

}
